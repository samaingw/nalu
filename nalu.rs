use std::sync::mpsc::{Sender, channel};
use std::process::{Stdio, Command};
use std::io::{BufRead, BufReader, Read, Write};
//use std::collections::HashSet;
use std::env;
use std::thread;
use std::os::unix::net::{UnixListener, UnixStream};

// From Rust CookBook
use std::str::FromStr;
use std::string::ToString;
use std::fmt;
use std::error::Error;


const SOCKET_PATH: &str = "/tmp/nalu-socket";
const MAX_VIEWS: usize = 10;

#[derive(Copy, Clone, Debug)]
struct TagSet {
    value: u64,
}

impl TagSet {
    fn union(&self, other : &Self) -> Self {
        Self{ value: self.value | other.value }
    }

    fn intersection(&self, other : &Self) -> Self {
        Self{ value: self.value & other.value }
    }

    fn symetric_difference(&self, other: &Self) -> Self {
        Self{ value: self.value ^ other.value }
    }

    fn neg(&self) -> Self {
        Self{ value: !self.value }
    }

    fn empty(&self) -> bool {
        self.value == 0
    }

    fn get_first_activated_tag(&self) -> Self {
        const OFFSET_LIMIT : u64 = 63; // TODO: align with size of TagSet
        for offset in 0..OFFSET_LIMIT {
            let tag = TagSet{value: 1 << offset};
            let intersection = self.intersection(&tag);
            if !intersection.empty() {
                return tag;
            }
        }
        Self{ value: 0 }
    }
}

impl ToString for TagSet {
    fn to_string(&self) -> String {
        format!("{:032b}", self.value)
    }
}

impl FromStr for TagSet {
    type Err = std::num::ParseIntError;

    fn from_str(tagset_string: &str) -> Result<Self, Self::Err> {
        let z = match tagset_string.strip_prefix("0x") {
            Some(tagset_hex_string) => u64::from_str_radix(tagset_hex_string, 16)?,
            None => match tagset_string.strip_prefix("0b") {
                    Some(tagset_bin_string) => u64::from_str_radix(tagset_bin_string, 2)?,
                    None => 1 << (u64::from_str_radix(tagset_string, 10)? -1)
                }
        };
        Ok(Self{value: z})
    }
}

#[derive(Debug, PartialEq)]
struct NodeId(u64);

impl FromStr for NodeId {
    type Err = std::num::ParseIntError;

    fn from_str(hex_code: &str) -> Result<Self, Self::Err> {
    
        let without_prefix = hex_code.trim_start_matches("0x");
        let z = u64::from_str_radix(without_prefix, 16)?;
        Ok(Self(z))
    }
}

impl ToString for NodeId {
    fn to_string(&self) -> String {
        format!("{:#x}", self.0)
    }
}

#[derive(Debug, PartialEq)]
struct ViewId(usize);

#[derive(Debug)]
enum ViewTarget {
    Id(ViewId),
    Focused,
}

impl FromStr for ViewTarget {
    type Err = std::num::ParseIntError;

    fn from_str(id: &str) -> Result<Self, Self::Err> {
    
        match id {
            "current" => Ok(Self::Focused),
            _ => {
                let z = usize::from_str_radix(id, 10)?;
                Ok(Self::Id(ViewId(z-1)))
            },
        }
    }
}

#[derive(Debug)]
struct Node {
    id: NodeId,
    tagset: TagSet,
}

#[derive(Copy, Clone, Debug)]
struct View {
    tagset: TagSet,
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct DesktopId(u64);

impl ToString for DesktopId {
    fn to_string(&self) -> String {
        format!("{:#x}", self.0)
    }
}

#[derive(Debug)]
enum DesktopTarget {
    Id(DesktopId),
    Focused,
}

impl FromStr for DesktopTarget {
    type Err = std::num::ParseIntError;

    fn from_str(target: &str) -> Result<Self, Self::Err> {

        match target {
            "focused" => Ok(Self::Focused),
            _ => {
                let without_prefix = target.trim_start_matches("0x");
                let z = u64::from_str_radix(without_prefix, 16)?;
                Ok(Self::Id(DesktopId(z)))
            },
        }
    }
}

#[derive(Debug)]
struct NaluDesktop {
    id: DesktopId,
    nodes: Vec<Node>,
    views: [View; MAX_VIEWS],
    current_view: ViewId,
    current_node: Option<usize>,
}

impl Node {
    fn show(&self) {
        Command::new("bspc")
        .arg("node")
        .arg(self.id.to_string())
        .arg("-g")
        .arg("hidden=off")
        .output()
        .unwrap();
    }

    fn hide(&self) {
        Command::new("bspc")
        .arg("node")
        .arg(self.id.to_string())
        .arg("-g")
        .arg("hidden=on")
        .output()
        .unwrap();
    }

    fn transfer_to_desktop(&self, desktop: &DesktopId) {
        Command::new("bspc")
        .arg("node")
        .arg(self.id.to_string())
        .arg("-d")
        .arg(desktop.to_string())
        .output()
        .unwrap();
    }
}

impl NaluDesktop {
    fn find_node_pos(&self, node_id: &NodeId) -> Option<usize> {
        self.nodes.iter().position(|n| &n.id == node_id)
    }

    fn add_node(&mut self, node: Node) -> usize {
        self.nodes.push(node);
        self.nodes.len() - 1
    }

    fn del_node(&mut self, node_index: usize) -> Node {
        if let Some(focused_node_index) = self.current_node {
            if focused_node_index == node_index {
                self.current_node = None;
            } else {
                // Index recomputation for focused node
                if focused_node_index == self.nodes.len()-1 {
                    // This will be the swapped node by swap_remove
                    self.current_node = Some(node_index);
                }
            }
        }
        // Actual removal
        self.nodes.swap_remove(node_index)
    }

    fn view_apply_tagset_change(& self, old_tagset: &TagSet, view_index: &ViewId) {
        // TODO: a way to compact this ?
        if view_index == &self.current_view {
            let new_tagset = &self.views[view_index.0].tagset;
            for node in self.nodes.iter() {
                let was_invisible = node.tagset.intersection(old_tagset).empty();
                let is_invisible = node.tagset.intersection(new_tagset).empty();
                if was_invisible != is_invisible {
                    if was_invisible {
                        node.show();
                    } else {
                        node.hide();
                    }
                }
            }
        }
    }

    fn view_toggle_tagset(&mut self, tags: TagSet, view_index: &ViewId) {
        // TODO: SAFETY !!!
        let view = &mut self.views[view_index.0];
        let old_tagset = TagSet{value: view.tagset.value};
        view.tagset = view.tagset.symetric_difference(&tags);

        self.view_apply_tagset_change(&old_tagset, view_index);
    }

    fn view_assign_tagset(&mut self, tags: TagSet, view_index: &ViewId) {
        let view = &mut self.views[view_index.0];
        let old_tagset = TagSet{value: view.tagset.value};
        view.tagset = tags;

        self.view_apply_tagset_change(&old_tagset, view_index);
    }

    fn view_switch(&mut self, new_current_view_index: ViewId) {
        let old_tagset = &self.views[self.current_view.0].tagset;
        self.current_view = new_current_view_index;

        self.view_apply_tagset_change(old_tagset, &self.current_view);
    }

    fn view_get_tagset(&self, view_index: &ViewId) -> TagSet {
        self.views[view_index.0].tagset
    }

    fn node_apply_tagset_change(&self, old_tagset: &TagSet, target_node: &Node, src_desktop: &NaluDesktop) {
        let new_tagset = &target_node.tagset;
        let current_view_tagset = &self.views[self.current_view.0].tagset;
        let previous_view_tagset = &src_desktop.views[src_desktop.current_view.0].tagset;
        let was_invisible = old_tagset.intersection(previous_view_tagset).empty();
        let is_invisible = new_tagset.intersection(current_view_tagset).empty();

        // 3 class of cases here:
        // 1) Was with some tags and is with some tags => apply the tagset intersection logic
        // 2) Was/is visible and is/was without tags => keep the same visibility (no-op)
        // 3) Was/is invisible and is/was without tags => even though it never intersects with the visible tagset, visibility should change

        let was_untagged = old_tagset.empty();
        let is_untagged = new_tagset.empty();

        //dbg!(was_invisible, is_invisible, was_untagged, is_untagged, previous_view_tagset, current_view_tagset);

        if was_untagged && is_untagged {
            // The only reasonable time when we enter here is when
            // we transfer/swap a node from a previous desktop.
            // Otherwise it shouldn't happen anyway because this means
            // that tags was empty
            // no-op here
        } else if was_untagged && is_invisible {
            // First part of case 3
            target_node.hide();
        } else if was_invisible && is_untagged {
            // Second part of case 3
            target_node.show();
        } else if !was_untagged && !is_untagged && was_invisible != is_invisible {
            // We're dealing with case 1 here
            if was_invisible {
                target_node.show();
            } else {
                target_node.hide();
            }
        }
    }

    fn node_toggle_tagset(&mut self, tags: TagSet, node_index: usize) {
        // TODO: Some clever index computation here (based on bsp's node
        // numerotation scheme).
        let target_node = &mut self.nodes[node_index];
        let old_tagset = target_node.tagset;
        let new_tagset = old_tagset.symetric_difference(&tags);

        target_node.tagset = new_tagset;
        // Dropping the mut
        let target_node = & self.nodes[node_index];
        self.node_apply_tagset_change(&old_tagset, target_node, self);
    }

    fn node_assign_tagset(&mut self, tags: TagSet, node_index: usize) {
        let target_node = &mut self.nodes[node_index];
        let old_tagset = target_node.tagset;
        let new_tagset = tags;

        target_node.tagset = new_tagset;
        // Dropping the mut
        let target_node = & self.nodes[node_index];
        self.node_apply_tagset_change(&old_tagset, target_node, self);

    }

    fn node_focus(&mut self, node: Option<usize>) {
        self.current_node = node;
    }

    fn show_all_nodes(&self) {
        let current_view_tagset = &self.views[self.current_view.0].tagset;
        for node in &self.nodes {
            let node_tagset = &node.tagset;
            if !node_tagset.empty() && node_tagset.intersection(current_view_tagset).empty() {
                // This means a managed node (in the sense that it has at least one tag)
                // is currently invisible => make it visible when nalu quits
                node.show();
            }
        }
    }

    fn get_filled_tags(&self) -> TagSet {
        let mut full_tagset = TagSet{value:0};
        for node in &self.nodes {
            full_tagset = full_tagset.union(&node.tagset);
        }
        full_tagset
    }

    fn node_assign_empty_tag_and_view_it(&mut self, node_index: usize, view_index: &ViewId) {
        let full_tagset = self.get_filled_tags();
        let candidates = full_tagset.neg();
        let tag = candidates.get_first_activated_tag();
        let new_view_tagset = self.views[view_index.0].tagset.union(&tag);
        self.view_assign_tagset(new_view_tagset, view_index);
        self.node_assign_tagset(tag, node_index);
    }

    fn remove_tagset_from_all_views(&mut self, tagset: TagSet) {
        for i in 0..MAX_VIEWS-1 {
            if ! (self.views[i].tagset.intersection(&tagset).empty()) {
                let view_id = ViewId(i);
                self.view_toggle_tagset(tagset, &view_id);
            }
        }
    }

    fn node_get_tagset(&self, node_index: usize) -> TagSet {
         self.nodes[node_index].tagset
    }
}

#[derive(Debug)]
struct NaluDisplay {
    desktops: Vec<NaluDesktop>,
    current_desktop: Option<usize>,
}

impl NaluDisplay {
    fn desktop_index(&self, desktop_target: &DesktopTarget) -> Option<usize> {
        match desktop_target {
            DesktopTarget::Focused => self.current_desktop,
            DesktopTarget::Id(desktop_id) => self.desktops.iter().position(|n| &n.id == desktop_id),
        }
    }

    fn desktop_add(&mut self, id: DesktopId) {
        let new_desktop = NaluDesktop{ id: id,
                                   nodes: vec!(),
                                   views: [View{tagset:TagSet{value:0}}; MAX_VIEWS],
                                   current_view: ViewId(0),
                                   current_node: None,
                                 };
        self.desktops.push(new_desktop);
    }

    fn desktop_del(&mut self, desktop_pos: usize) -> NaluDesktop {
        // Bspwm takes care of transfering nodes when a desktop
        // is removed. So, in the normal process, the nodes
        // have already been transferred.
        // EXCEPT
        // When bswpm is restarted.
        // In this case the desktop is removed without any node
        // transfer => we should make the windows visible again.
        self.desktops[desktop_pos].show_all_nodes();
        if let Some(focused_desktop) = self.current_desktop {
            if focused_desktop == desktop_pos {
                self.current_desktop = None;
            } else {
                // Index recomputation for focused desktop
                if focused_desktop == self.desktops.len()-1 {
                    // This will be the swapped desktop by swap_remove
                    self.current_desktop = Some(desktop_pos);
                }
            }

        }
        self.desktops.swap_remove(desktop_pos)
    }

    fn desktop_focus(&mut self, desktop_pos: Option<usize>) {
        self.current_desktop = desktop_pos;
    }
}

#[derive(Debug)]
enum NaluMode {
    NoTag,     // Entirely manual mode, created nodes have no tags => they are not managed.
    ViewTag,   // Created nodes are assigned the tags of the current view (similar to dwm way).
    EmptyTag,  // Created nodes are assigned the first empty tag of the current desktop.
    UniqueTag, // Created nodes are assigned the first empty tag across _all_ desktops.
               // Moreover, in UniqueTag, nodes are moved to desktops which view(s) intersect
               // with the node's tagset (this is similar to velox way).
}

#[derive(Debug)]
struct DumbError;

impl fmt::Display for DumbError{
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "A dumb error occurred")
    }
}

impl Error for DumbError{}

impl FromStr for NaluMode {
    type Err = DumbError;

    fn from_str(new_mode: &str) -> Result<Self, Self::Err> {
        match new_mode {
            "notag" => Ok(Self::NoTag),
            "viewtag" => Ok(Self::ViewTag),
            "emptytag" => Ok(Self::EmptyTag),
            "uniquetag" => Ok(Self::UniqueTag),
            _ => Err(DumbError{}),
        }
    }
}

impl ToString for NaluMode {
    fn to_string(&self) -> String {
        match self {
            Self::NoTag => "NoTag".to_string(),
            Self::ViewTag => "ViewTag".to_string(),
            Self::EmptyTag => "EmptyTag".to_string(),
            Self::UniqueTag => "UniqueTag".to_string(),
        }
    }
}
// Copyright (C) Cedric Barreteau 2020-...
// This is a big chunk of naaw's code

#[derive(Debug)]
enum Event {
    AddNode(DesktopTarget, NodeId),
    RemoveNode(DesktopTarget, NodeId),
    SwapNode(DesktopTarget, NodeId, DesktopTarget, NodeId),
    TransferNode(DesktopTarget, NodeId, DesktopTarget),
    FocusNode(DesktopTarget, NodeId),
    TagsetNode(DesktopTarget, NodeId, TagSet),
    ToggleTagsetNode(DesktopTarget, NodeId, TagSet),
    EmptyTagNode(DesktopTarget, NodeId, ViewTarget),
    TagsetView(DesktopTarget, ViewTarget, TagSet),
    ToggleTagsetView(DesktopTarget, ViewTarget, TagSet),
    ChangeView(DesktopTarget, ViewTarget),
    AddDesktop(DesktopId),
    RemoveDesktop(DesktopTarget),
    FocusDesktop(DesktopTarget),
    ChangeMode(NaluMode),
    NaluQuit,
    QueryTag(DesktopTarget, NodeId, UnixStream),
    QueryView(DesktopTarget, ViewTarget, UnixStream),
    QueryActive(DesktopTarget, UnixStream),
    QueryMode(UnixStream),
    QueryCurrentView(DesktopTarget, UnixStream),
}

// This could we useful if we use a Result<> instead of Option<> for BspcSubCommand event parsing.
// However, as we are now using it, trying a parser after the previous one failed, Option<> was
// preferred.
//enum EventErr {
//     BspcConvertError
//}

//impl Event {
//    fn from_bspc(sub_command: &BspcSubCommand, desktop_id: &str, node_id: &str) -> Result<Self, <NodeId as FromStr>::Err> {
//        let node = NodeId::from_str(node_id)?;
//        match sub_command {
//            BspcSubCommand::NodeAdd => Ok(Self::AddNode(DesktopTarget::Focused, node)),
//            BspcSubCommand::NodeRemove => Ok(Self::RemoveNode(DesktopTarget::Focused, node))
//        }
//    }
//}
//

#[derive(Debug)]
enum BspcSubCommand {
    NodeAdd,
    NodeRemove,
    NodeSwap,
    NodeTransfer,
    NodeFocus,
    DesktopAdd,
    DesktopRemove,
    DesktopFocus
}

impl BspcSubCommand {
    fn name(&self) -> &str {
        match self {
           Self::NodeAdd => "node_add",
           Self::NodeRemove => "node_remove",
           Self::NodeSwap => "node_swap",
           Self::NodeTransfer => "node_transfer",
           Self::NodeFocus => "node_focus",
           Self::DesktopAdd => "desktop_add",
           Self::DesktopRemove => "desktop_remove",
           Self::DesktopFocus => "desktop_focus",
        }
    }

    fn parse(&self, line: &str) -> Option<Event> {
        match self {
            Self::NodeAdd => {
                let desktop_index : usize = 2;
                let node_index : usize = 4;
                if let Ok(desktop_target) = DesktopTarget::from_str(line.split(' ').nth(desktop_index)?) {
                    if let Ok(node_target) = NodeId::from_str(line.split(' ').nth(node_index)?) {
                        //return Some(Event::AddNode(desktop_target, node_target));
                        return Some(Event::AddNode(desktop_target, node_target));
                    }
                }
                None
            },
            Self::NodeRemove => {
                let desktop_index : usize = 2;
                let node_index : usize = 3;
                if let Ok(desktop_target) = DesktopTarget::from_str(line.split(' ').nth(desktop_index)?) {
                    if let Ok(node_target) = NodeId::from_str(line.split(' ').nth(node_index)?) {
                        //return Some(Event::RemoveNode(desktop_target, node_target));
                        return Some(Event::RemoveNode(desktop_target, node_target));
                    }
                }
                None
            },
            Self::NodeSwap => {
                let src_desktop_index : usize = 2;
                let dst_desktop_index : usize = 5;
                let src_node_index : usize = 3;
                let dst_node_index : usize = 6;
                if let Ok(src_desktop_target) = DesktopTarget::from_str(line.split(' ').nth(src_desktop_index)?) {
                    if let Ok(dst_desktop_target) = DesktopTarget::from_str(line.split(' ').nth(dst_desktop_index)?) {
                        if let Ok(src_node_target) = NodeId::from_str(line.split(' ').nth(src_node_index)?) {
                            if let Ok(dst_node_target) = NodeId::from_str(line.split(' ').nth(dst_node_index)?) {
                                return Some(Event::SwapNode(src_desktop_target, src_node_target, dst_desktop_target, dst_node_target));
                            }
                        }
                    }
                }
                None
            }
            Self::NodeTransfer => {
                let src_desktop_index : usize = 2;
                let dst_desktop_index : usize = 5;
                let src_node_index : usize = 3;
                if let Ok(src_desktop_target) = DesktopTarget::from_str(line.split(' ').nth(src_desktop_index)?) {
                    if let Ok(dst_desktop_target) = DesktopTarget::from_str(line.split(' ').nth(dst_desktop_index)?) {
                        if let Ok(src_node_target) = NodeId::from_str(line.split(' ').nth(src_node_index)?) {
                            return Some(Event::TransferNode(src_desktop_target, src_node_target, dst_desktop_target));
                        }
                    }
                }
                None
            }
            Self::NodeFocus => {
                let desktop_index : usize = 2;
                let node_index : usize = 3;
                if let Ok(desktop_target) = DesktopTarget::from_str(line.split(' ').nth(desktop_index)?) {
                    if let Ok(node_target) = NodeId::from_str(line.split(' ').nth(node_index)?) {
                        return Some(Event::FocusNode(desktop_target, node_target));
                    }
                }
                None
            }
            Self::DesktopAdd => {
                let desktop_index : usize = 2;
                if let Ok(DesktopTarget::Id(desktop_id)) = DesktopTarget::from_str(line.split(' ').nth(desktop_index)?) {
                    return Some(Event::AddDesktop(desktop_id));
                }
                None
            },
            Self::DesktopRemove => {
                let desktop_index : usize = 2;
                if let Ok(desktop_target) = DesktopTarget::from_str(line.split(' ').nth(desktop_index)?) {
                    return Some(Event::RemoveDesktop(desktop_target));
                }
                None
            }
            Self::DesktopFocus => {
                let desktop_index : usize = 2;
                if let Ok(desktop_target) = DesktopTarget::from_str(line.split(' ').nth(desktop_index)?) {
                    return Some(Event::FocusDesktop(desktop_target));
                }
                None
            }
        }
    }
}

// That is still useful if someone wishes to avoid desktop awareness for nalu.
fn init_nodes_globally(tx: Sender<Event>) {
    thread::spawn(move || {
        let output = Command::new("bspc")
            .arg("query")
            .arg("-N")
            .arg("-n")
            .arg(".window")
            .stdout(Stdio::piped())
            .spawn()
            .unwrap()
            .stdout
            .unwrap();
        for line in BufReader::new(output).lines() {
            let line = match line {
                Err(err) => {
                    eprintln!("{}", err.to_string());
                    continue
                },
                Ok(l) => l,
            };
            if let Ok(node_id) = NodeId::from_str(&line) {
                let desktop_target = DesktopTarget::Focused;
                let ev = Event::AddNode(desktop_target, node_id);
                if let Err(err) = tx.send(ev) {
                    eprintln!("{}", err.to_string());
                    continue
                }
            }
        }
    });
}

fn init_nodes_inside_desktop(tx: Sender<Event>, desktop_id: DesktopId) {
    thread::spawn(move || {
        let output = Command::new("bspc")
            .arg("query")
            .arg("-N")
            .arg("-n")
            .arg(".window")
            .arg("-d")
            .arg(desktop_id.to_string())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap()
            .stdout
            .unwrap();
        for line in BufReader::new(output).lines() {
            let line = match line {
                Err(err) => {
                    eprintln!("{}", err.to_string());
                    continue
                },
                Ok(l) => l,
            };
            if let Ok(node_id) = NodeId::from_str(&line) {
            let desktop_target = DesktopTarget::Id(desktop_id);
                let ev = Event::AddNode(desktop_target, node_id);
                if let Err(err) = tx.send(ev) {
                    eprintln!("{}", err.to_string());
                    continue
                }
            }
        }
    });
}

fn init_desktops_globally(tx: Sender<Event>) {
    thread::spawn(move || {
        let output = Command::new("bspc")
            .arg("query")
            .arg("-D")
            .stdout(Stdio::piped())
            .spawn()
            .unwrap()
            .stdout
            .unwrap();
        for line in BufReader::new(output).lines() {
            let line = match line {
                Err(err) => {
                    eprintln!("{}", err.to_string());
                    continue
                },
                Ok(l) => l,
            };
            if let Ok(DesktopTarget::Id(desktop_id)) = DesktopTarget::from_str(&line) {
                let ev = Event::AddDesktop(desktop_id);
                if let Err(err) = tx.send(ev) {
                    eprintln!("{}", err.to_string());
                    continue
                }
                // Now, add the nodes inside this desktop
                init_nodes_inside_desktop(tx.clone(), desktop_id);
                let focused_node_output = Command::new("bspc")
                    .arg("query")
                    .arg("-N")
                    .arg("-d")
                    .arg(desktop_id.to_string())
                    .output()
                    .unwrap();
                let node_str = std::str::from_utf8(focused_node_output.stdout.as_slice()).unwrap().trim();
                if let Ok(node_id) = NodeId::from_str(node_str) {
                    let ev = Event::FocusNode(DesktopTarget::Id(desktop_id), node_id);
                    if let Err(err) = tx.send(ev) {
                       eprintln!("{}", err.to_string());
                    }
                }
            }

        }
        let output = Command::new("bspc")
            .arg("query")
            .arg("-D")
            .arg("-d")
            .arg("focused")
            .output()
            .unwrap();
        let desktop_str = std::str::from_utf8(output.stdout.as_slice()).unwrap().trim();
        if let Ok(desktop_target) = DesktopTarget::from_str(desktop_str) {
            let ev = Event::FocusDesktop(desktop_target);
            if let Err(err) = tx.send(ev) {
                eprintln!("{}", err.to_string());
            }
        }
    });

}

fn init(tx: Sender<Event>) {
//    init_nodes_globally(tx);
    init_desktops_globally(tx);
}

fn subscribe_bspc_array(sub_command_list: Vec<BspcSubCommand>, tx: Sender<Event>) {
    thread::spawn(move || {
        let mut output = Command::new("bspc");
        output.arg("subscribe");
        for sub_command in &sub_command_list {
            output.arg(sub_command.name());
        }
        let output = output.stdout(Stdio::piped())
            .spawn()
            .unwrap()
            .stdout
            .unwrap();
        for line in BufReader::new(output).lines() {
            let line = match line {
                Err(err) => {
                    eprintln!("{}", err.to_string());
                    continue
                },
                Ok(l) => l,
            };
            let sub_name = match line.split(' ').nth(0) {
                Some(name) => name,
                None => {
                    eprintln!("Bspc event without any word !");
                    continue
                }
            };
            // Identifying the corresponding subscribe command
            for sub_command in &sub_command_list {
                if sub_command.name() == sub_name {
                    // Parsing the event
                    match sub_command.parse(&line) {
                        Some(ev) => {
                            if let Err(err) = tx.send(ev) {
                                eprintln!("{}", err.to_string());
                            }
                        },
                        None => eprintln!("Couldn't parse bspc output"),
                    }
                }
            }
        }
        let ev = Event::NaluQuit;
        if let Err(err) = tx.send(ev) {
            eprintln!("{}", err.to_string());
        }
    });
}

fn handle_client_stream(mut stream: UnixStream, tx: Sender<Event>) {
    let mut message_bytes = [0; 128];
    let count = stream.read(&mut message_bytes).unwrap();
    let message = String::from_utf8(Vec::from(&message_bytes[0..count])).unwrap();
    let message_words : Vec<&str> = match message.strip_suffix('\n') {
        Some(msg) => msg,
        None => message.as_str()
    }.split(' ').collect();

    match message_words[0] {
        "tag" =>  {
            let node_str = message_words[1];
            let tagset_str = message_words[2];
            if let Ok(node_id) = NodeId::from_str(node_str) {
                if let Ok(tagset) = TagSet::from_str(tagset_str) {
                    if let Err(err) = tx.send(Event::TagsetNode(DesktopTarget::Focused, node_id, tagset)) {
                        eprintln!("{}", err.to_string());
                    }
                }
            }
        }
        "toggletag" => {
            let node_str = message_words[1];
            let tagset_str = message_words[2];
            if let Ok(node_id) = NodeId::from_str(node_str) {
                if let Ok(tagset) = TagSet::from_str(tagset_str) {
                    if let Err(err) = tx.send(Event::ToggleTagsetNode(DesktopTarget::Focused, node_id, tagset)) {
                        eprintln!("{}", err.to_string());
                    }
                }
            }
        }
        "nextempty" => {
            let node_str = message_words[1];
            let view_str = message_words[2];
            if let Ok(node_id) = NodeId::from_str(node_str) {
                if let Ok(view_target) = ViewTarget::from_str(view_str) {
                    if let Err(err) = tx.send(Event::EmptyTagNode(DesktopTarget::Focused, node_id, view_target)) {
                        eprintln!("{}", err.to_string());
                    }
                }
            }
        }
        "view" => {
            let view_str = message_words[1];
            let tagset_str = message_words[2];
            if let Ok(view_target) = ViewTarget::from_str(view_str) {
                if let Ok(tagset) = TagSet::from_str(tagset_str) {
                    if let Err(err) = tx.send(Event::TagsetView(DesktopTarget::Focused, view_target, tagset)) {
                        eprintln!("{}", err.to_string());
                    }
                }
            }
        }
        "toggleview" => {
            let view_str = message_words[1];
            let tagset_str = message_words[2];
            if let Ok(view_target) = ViewTarget::from_str(view_str) {
                if let Ok(tagset) = TagSet::from_str(tagset_str) {
                    if let Err(err) = tx.send(Event::ToggleTagsetView(DesktopTarget::Focused, view_target, tagset)) {
                        eprintln!("{}", err.to_string());
                    }
                }
            }
        }
        "switch" => {
            let view_str = message_words[1];
            if let Ok(view_target) = ViewTarget::from_str(view_str) {
                if let Err(err) = tx.send(Event::ChangeView(DesktopTarget::Focused, view_target)) {
                    eprintln!("{}", err.to_string());
                }
            }
        }
        "mode" => {
            let mode_str = message_words[1];
            if let Ok(new_mode) = NaluMode::from_str(mode_str) {
                if let Err(err) = tx.send(Event::ChangeMode(new_mode)) {
                    eprintln!("{}", err.to_string());
                }
            }
        }
        "quit" => {
                if let Err(err) = tx.send(Event::NaluQuit) {
                    eprintln!("{}", err.to_string());
                }
        }
        "ping" => {
            // Note that this is the client stream manager thread
            // which is answering here, not the state server thread.
            // As such, it can be used to check if the stream manager
            // is still alive, but it won't be of any use to diagnose
            // if the state thread hung up.
            if let Err(err) = stream.write_all(b"Pong!") {
                eprintln!("{}", err.to_string());
            }
        }
        "querytag" => {
            let node_str = message_words[1];
            if let Ok(node_id) = NodeId::from_str(node_str) {
                if let Err(err) = tx.send(Event::QueryTag(DesktopTarget::Focused, node_id, stream)) {
                    eprintln!("{}", err.to_string());
                }
            }
        }
        "queryview" => {
            let view_str = message_words[1];
            if let Ok(view_target) = ViewTarget::from_str(view_str) {
                if let Err(err) = tx.send(Event::QueryView(DesktopTarget::Focused, view_target, stream)) {
                    eprintln!("{}", err.to_string());
                }
            }
        }
        "queryactive" => {
            if let Err(err) = tx.send(Event::QueryActive(DesktopTarget::Focused, stream)) {
                eprintln!("{}", err.to_string());
            }
        }
        "querymode" => {
            if let Err(err) = tx.send(Event::QueryMode(stream)) {
                eprintln!("{}", err.to_string());
            }
        }
        "querycurrentview" => {
            if let Err(err) = tx.send(Event::QueryCurrentView(DesktopTarget::Focused, stream)) {
                eprintln!("{}", err.to_string());
            }
        }
        _ => {
            eprintln!("Unsupported message {}", message);
        }
    }
    
}

fn subscribe_client(tx: Sender<Event>) {
    let _ = std::fs::remove_file(SOCKET_PATH);
    let listener = UnixListener::bind(SOCKET_PATH).unwrap();
    thread::spawn(move || {
        for stream in listener.incoming() {
            match stream {
                Ok(stream) => handle_client_stream(stream, tx.clone()),
                Err(err) => {
                    eprintln!("{}", err.to_string());
                    continue
                }
            }
        }
    });
}

fn server() {
    let (tx, rx) = channel::<Event>();
    //let mut state = State::new(ing)
    let mut state = NaluDisplay{ desktops: vec!(
            //NaluDesktop{id:DesktopId(0), nodes: vec!(), views:[View{tagset:TagSet{tagset:0}}; MAX_VIEWS], current_view:ViewId(0)}
            ), current_desktop: None};
    init(tx.clone());
    subscribe_bspc_array(vec!(
            BspcSubCommand::NodeAdd,
            BspcSubCommand::NodeRemove,
            BspcSubCommand::NodeSwap,
            BspcSubCommand::NodeTransfer,
            BspcSubCommand::NodeFocus,
            BspcSubCommand::DesktopAdd,
            BspcSubCommand::DesktopRemove,
            BspcSubCommand::DesktopFocus,
            ), tx.clone());
    subscribe_client(tx);

    let mut mode = NaluMode::EmptyTag;

    for state_change in &rx {
        //dbg!(&state_change);
        match state_change {
            Event::AddDesktop(desktop_id) => {
                state.desktop_add(desktop_id);
            }
            Event::RemoveDesktop(desktop_target) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    state.desktop_del(desktop_index);
                }
            }
            Event::FocusDesktop(desktop_target) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    state.desktop_focus(Some(desktop_index));
                }
            }
            Event::AddNode(desktop_target, node_id) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    // FIXME: rust is unhappy with the code below because it borrows
                    // mutably state, which is immutably borrowed later in NaluMode::UniqueTag pattern.
                    // let desktop = &mut state.desktops[desktop_index];
                    let node = Node{id: node_id, tagset: TagSet{value: 0}};
                    let new_node_index = state.desktops[desktop_index].add_node(node);
                    match mode {
                        NaluMode::NoTag => (),
                        NaluMode::ViewTag => {
                            let desktop = &mut state.desktops[desktop_index];
                            let current_view_id = ViewId(desktop.current_view.0);
                            let current_view_tagset = desktop.views[current_view_id.0].tagset;
                            desktop.node_assign_tagset(current_view_tagset, new_node_index);
                        }
                        NaluMode::EmptyTag => {
                            let desktop = &mut state.desktops[desktop_index];
                            let current_view_id = ViewId(desktop.current_view.0);
                            desktop.node_assign_empty_tag_and_view_it(new_node_index, &current_view_id);
                            // let full_tagset = desktop.get_filled_tags();
                            // let candidates = full_tagset.neg();
                            // let tag = candidates.get_first_activated_tag();
                            // let new_view_tagset = desktop.views[view_id.0].tagset.union(&tag);
                            // desktop.view_assign_tagset(new_view_tagset, &view_id);
                            // desktop.node_assign_tagset(tag, new_node_index);
                        }
                        NaluMode::UniqueTag => {
                            let mut full_tagset_all_desktops = TagSet{ value: 0 };
                            for one_desktop in &state.desktops {
                                full_tagset_all_desktops = full_tagset_all_desktops.union(&one_desktop.get_filled_tags());
                            }
                            let tag = full_tagset_all_desktops.neg().get_first_activated_tag();
                            let desktop = &mut state.desktops[desktop_index];
                            let current_view_id = ViewId(desktop.current_view.0);
                            let new_view_tagset = desktop.views[current_view_id.0].tagset.union(&tag);
                            desktop.view_assign_tagset(new_view_tagset, &current_view_id);
                            desktop.node_assign_tagset(tag, new_node_index);
                        }
                    }
                }
            }
            Event::RemoveNode(desktop_target, node_id) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &mut state.desktops[desktop_index];
                    if let Some(node_pos) = desktop.find_node_pos(&node_id) {
                        let node = desktop.del_node(node_pos);
                        match mode {
                            NaluMode::NoTag | NaluMode::ViewTag => (),
                            NaluMode::EmptyTag | NaluMode::UniqueTag => {
                                desktop.remove_tagset_from_all_views(node.tagset);
                                // In UniqueTag mode, technically, we should remove node.tagset
                                // from _all_ desktops. But the move semantic ensure node.tagset
                                // is present only on one given desktop, the one holding the node.
                            },
                        }
                    }
                }
            }
            Event::SwapNode(src_desktop_target, src_node_id, dst_desktop_target, dst_node_id) => {
                if let Some(src_desktop_index) = state.desktop_index(&src_desktop_target) {
                if let Some(dst_desktop_index) = state.desktop_index(&dst_desktop_target) {
                    let desktops = &mut state.desktops;
                    let sdi = src_desktop_index;
                    let ddi = dst_desktop_index;
                    if let Some(src_node_pos) = desktops[sdi].find_node_pos(&src_node_id) {
                    if let Some(dst_node_pos) = desktops[ddi].find_node_pos(&dst_node_id) {
                        let src_node = desktops[sdi].del_node(src_node_pos);
                        desktops[ddi].node_apply_tagset_change(&src_node.tagset, &src_node, &desktops[sdi]);
                        desktops[ddi].add_node(src_node);
                        let dst_node = desktops[ddi].del_node(dst_node_pos);
                        desktops[sdi].node_apply_tagset_change(&dst_node.tagset, &dst_node, &desktops[ddi]);
                        desktops[sdi].add_node(dst_node);
                    }
                    }
                }
                }
            }
            Event::TransferNode(src_desktop_target, src_node_id, dst_desktop_target) => {
                if let Some(src_desktop_index) = state.desktop_index(&src_desktop_target) {
                if let Some(dst_desktop_index) = state.desktop_index(&dst_desktop_target) {
                   let desktops = &mut state.desktops;
                    let sdi = src_desktop_index;
                    let ddi = dst_desktop_index;
                    if let Some(src_node_pos) = desktops[sdi].find_node_pos(&src_node_id) {
                        match mode {
                            NaluMode::NoTag | NaluMode::ViewTag | NaluMode::EmptyTag => (),
                            NaluMode::UniqueTag => {
                                if sdi != ddi {
                                    // Remove the node's tagset from the old desktop's views
                                    let node_tagset = desktops[sdi].nodes[src_node_pos].tagset;
                                    desktops[sdi].remove_tagset_from_all_views(node_tagset);
                                }
                            },
                        }
                        let src_node = desktops[sdi].del_node(src_node_pos);
                        desktops[ddi].node_apply_tagset_change(&src_node.tagset, &src_node, &desktops[sdi]);
                        desktops[ddi].add_node(src_node);
                    }
                    else {
                        // This happens if a desktop is removed.
                        // In this case, the whole tree under
                        // that desktop is transfered to another
                        // one, which means the node_transfer event
                        // targets the tree root node, which is a window
                        // only if there is one and only one window.
                        // As nalu catches only window nodes,
                        // we have to grab the windows under the
                        // root to properly transfer them.

                        let output = Command::new("bspc")
                            .arg("query")
                            .arg("-N")
                            .arg(src_node_id.to_string())
                            .arg("-n")
                            .arg(".descendant_of.window")
                            .stdout(Stdio::piped())
                            .spawn()
                            .unwrap()
                            .stdout
                            .unwrap();
                        for line in BufReader::new(output).lines() {
                            let line = match line {
                                Err(err) => {
                                    eprintln!("{}", err.to_string());
                                    continue
                                },
                                Ok(l) => l,
                            };
                            if let Ok(leaf_node_id) = NodeId::from_str(&line) {
                                dbg!(&leaf_node_id);
                                if let Some(src_node_pos) = desktops[sdi].find_node_pos(&leaf_node_id) {
                                    dbg!(src_node_pos);
                                    let src_node = desktops[sdi].del_node(src_node_pos);
                                    desktops[ddi].node_apply_tagset_change(&src_node.tagset, &src_node, &desktops[sdi]);
                                    desktops[ddi].add_node(src_node);
                                }
                            }
                        }
                    }
                }
                }
            }
            Event::FocusNode(desktop_target, node_id) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &mut state.desktops[desktop_index];
                    let node_pos = desktop.find_node_pos(&node_id);
                    desktop.node_focus(node_pos);
                }
            }
            Event::TagsetNode(desktop_target, node_id, tagset) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    // let desktop = &mut state.desktops[desktop_index];
                    let node_pos = state.desktops[desktop_index].find_node_pos(&node_id);
                    match node_pos {
                        Some(pos) => {
                            match mode {
                                NaluMode::NoTag | NaluMode::ViewTag | NaluMode::EmptyTag => state.desktops[desktop_index].node_assign_tagset(tagset, pos),
                                NaluMode::UniqueTag => {
                                    // Check if the tagset is already in use.
                                    // If it is in use, even partly, abort.
                                    let mut full_tagset_all_desktops = TagSet{ value: 0 };
                                    for one_desktop in &state.desktops {
                                        full_tagset_all_desktops = full_tagset_all_desktops.union(&one_desktop.get_filled_tags());
                                    }
                                    if full_tagset_all_desktops.intersection(&tagset).empty() {
                                        // The new tagset isn't in use
                                        state.desktops[desktop_index].node_assign_tagset(tagset, pos);
                                    }
                                },
                            }
                        },
                        None => ()
                    };
                }
            }
            Event::ToggleTagsetNode(desktop_target, node_id, tagset) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &mut state.desktops[desktop_index];
                    let node_pos = desktop.find_node_pos(&node_id);
                    match node_pos {
                        Some(pos) => {
                            match mode {
                                NaluMode::NoTag | NaluMode::ViewTag | NaluMode::EmptyTag => state.desktops[desktop_index].node_toggle_tagset(tagset, pos),
                                NaluMode::UniqueTag => {
                                    // Check if the tagset is already in use.
                                    // If it is in use, even partly, abort.
                                    let mut full_tagset_all_desktops = TagSet{ value: 0 };
                                    for one_desktop in &state.desktops {
                                        full_tagset_all_desktops = full_tagset_all_desktops.union(&one_desktop.get_filled_tags());
                                    }
                                    // !!!! FIXME: Breaking the semantic here !!!!
                                    let new_tagset = state.desktops[desktop_index].nodes[pos].tagset.symetric_difference(&tagset);
                                    // !!!!!!!!!!!
                                    if full_tagset_all_desktops.intersection(&new_tagset).empty() {
                                        // The new tagset isn't in use
                                        state.desktops[desktop_index].node_toggle_tagset(tagset, pos);
                                    }
                                },
                            }
                        }
                        None => ()
                    };
                }
            }
            Event::EmptyTagNode(desktop_target, node_id, view) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &mut state.desktops[desktop_index];
                    let node_pos = desktop.find_node_pos(&node_id);
                    match node_pos {
                        Some(pos) => {
                            let effective_view = match view {
                                ViewTarget::Focused => ViewId(desktop.current_view.0),
                                ViewTarget::Id(view) => view,
                            };
                            desktop.node_assign_empty_tag_and_view_it(pos, &effective_view);
                            },
                        None => ()
                    };
                }
            }
            Event::TagsetView(desktop_target, view, tagset) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    // let desktop = &mut state.desktops[desktop_index];
                    let effective_view = match view {
                        ViewTarget::Focused => ViewId(state.desktops[desktop_index].current_view.0),
                        ViewTarget::Id(view) => view,
                    };
                    match mode {
                        NaluMode::NoTag | NaluMode::ViewTag | NaluMode::EmptyTag => state.desktops[desktop_index].view_assign_tagset(tagset, &effective_view),
                        NaluMode::UniqueTag => {
                            // Check if we move some nodes !
                            for one_desktop in &state.desktops {
                                for node in &one_desktop.nodes {
                                    if one_desktop.id != state.desktops[desktop_index].id && !node.tagset.intersection(&tagset).empty() {
                                        // Transfer node from one_desktop to desktops[desktop_index]
                                        node.transfer_to_desktop(&state.desktops[desktop_index].id);
                                    }
                                }
                            }
                            state.desktops[desktop_index].view_assign_tagset(tagset, &effective_view);
                        }
                    }
                }
            }
            Event::ToggleTagsetView(desktop_target, view, tagset) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &mut state.desktops[desktop_index];
                    let effective_view = match view {
                        ViewTarget::Focused => ViewId(desktop.current_view.0),
                        ViewTarget::Id(view) => view,
                    };
                    match mode {
                        NaluMode::NoTag | NaluMode::ViewTag | NaluMode::EmptyTag => desktop.view_toggle_tagset(tagset, &effective_view),
                        NaluMode::UniqueTag => {
                            // Check if we move some nodes !

                            // !!!! FIXME: Breaking the semantic here !!!!
                            let new_tagset = state.desktops[desktop_index].views[effective_view.0].tagset.symetric_difference(&tagset);
                            // !!!!!!!!!!!

                            for one_desktop in &state.desktops {
                                for node in &one_desktop.nodes {
                                    if one_desktop.id != state.desktops[desktop_index].id && !node.tagset.intersection(&new_tagset).empty() {
                                        // Transfer node from one_desktop to desktops[desktop_index]
                                        node.transfer_to_desktop(&state.desktops[desktop_index].id);
                                    }
                                }
                            }
                            state.desktops[desktop_index].view_toggle_tagset(tagset, &effective_view);
                        },
                    }
                }
            }
            Event::ChangeView(desktop_target, view) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &mut state.desktops[desktop_index];
                    let effective_view = match view {
                        ViewTarget::Focused => ViewId(desktop.current_view.0),
                        ViewTarget::Id(view) => view,
                    };
                    desktop.view_switch(effective_view);
                }
            }
            Event::ChangeMode(new_mode) => {
                mode = new_mode;
            }
            Event::NaluQuit => {
                for desk in &state.desktops {
                    desk.show_all_nodes();
                }
                return;
            }
            Event::QueryTag(desktop_target, node_id, mut stream) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &state.desktops[desktop_index];
                    let node_pos = desktop.find_node_pos(&node_id);
                    match node_pos {
                        Some(pos) => {
                            let node_tagset = desktop.node_get_tagset(pos);
                            if let Err(err) = stream.write_all(node_tagset.to_string().as_bytes()) {
                                eprintln!("{}", err.to_string());
                            }
                        },
                        None => ()
                    };
                }
            }
            Event::QueryView(desktop_target, view, mut stream) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &state.desktops[desktop_index];
                    let effective_view = match view {
                        ViewTarget::Focused => ViewId(desktop.current_view.0),
                        ViewTarget::Id(view) => view,
                    };
                    let view_tagset = desktop.view_get_tagset(&effective_view);
                    if let Err(err) = stream.write_all(view_tagset.to_string().as_bytes()) {
                        eprintln!("{}", err.to_string());
                    }
                }
            }
            Event::QueryActive(desktop_target, mut stream) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &state.desktops[desktop_index];
                    let active_tagset = desktop.get_filled_tags();
                    if let Err(err) = stream.write_all(active_tagset.to_string().as_bytes()) {
                        eprintln!("{}", err.to_string());
                    }
                }
            }
            Event::QueryMode(mut stream) => {
                    if let Err(err) = stream.write_all(mode.to_string().as_bytes()) {
                        eprintln!("{}", err.to_string());
                    }
            }
            Event::QueryCurrentView(desktop_target, mut stream) => {
                if let Some(desktop_index) = state.desktop_index(&desktop_target) {
                    let desktop = &state.desktops[desktop_index];
                    let view_index = desktop.current_view.0+1;
                    if let Err(err) = stream.write_all(format!("{}", view_index).as_bytes()) {
                        eprintln!("{}", err.to_string());
                    }
                }
            }
        };
        //dbg!(&state);
        if let Some(focused_desktop_index) = state.current_desktop {
            let focused_desktop = &state.desktops[focused_desktop_index];
            if let Some(focused_node_index) = focused_desktop.current_node {
                let focused_node = &focused_desktop.nodes[focused_node_index];
                println!("{} {}v{} n{} t{}",
                         mode.to_string(),
                         focused_desktop.current_view.0+1,
                         focused_desktop.views[focused_desktop.current_view.0].tagset.to_string(),
                         focused_node.tagset.to_string(),
                         focused_desktop.get_filled_tags().to_string()
                        );
            } else {
                println!("{} {}v{} n{} t{}",
                         mode.to_string(),
                         focused_desktop.current_view.0+1,
                         focused_desktop.views[focused_desktop.current_view.0].tagset.to_string(),
                         TagSet{value: 0}.to_string(),
                         focused_desktop.get_filled_tags().to_string()
                        );
            }
        }
    }
}

fn send_client_message(message: &str, wait_for_resp: bool) {
    let mut stream = UnixStream::connect(SOCKET_PATH).unwrap();
    stream.write_all(message.as_bytes()).unwrap();
    if wait_for_resp {
        let mut response = String::new();
        stream.read_to_string(&mut response).unwrap();
        println!("{}", response);
    }
}

fn tag(tagnum: &String) {
    let output = Command::new("bspc")
        .arg("query")
        .arg("-N")
        .arg("focused")
        .arg("-n")
        .output()
        .unwrap();
    let node = std::str::from_utf8(output.stdout.as_slice()).unwrap().trim();
    send_client_message(&format!("tag {} {}", node, tagnum), false);
}

fn toggletag(tagnum: &String) {
    let output = Command::new("bspc")
        .arg("query")
        .arg("-N")
        .arg("focused")
        .arg("-n")
        .output()
        .unwrap();
    let node = std::str::from_utf8(output.stdout.as_slice()).unwrap().trim();
    send_client_message(&format!("toggletag {} {}", node, tagnum), false);
}

fn nextemptytag() {
    let output = Command::new("bspc")
        .arg("query")
        .arg("-N")
        .arg("focused")
        .arg("-n")
        .output()
        .unwrap();
    let node = std::str::from_utf8(output.stdout.as_slice()).unwrap().trim();
    let view = "current";
    send_client_message(&format!("nextempty {} {}", node, view), false);
}

fn view(tagnum: &String) {
    let view = "current";
    send_client_message(&format!("view {} {}", view, tagnum), false);
}

fn toggleview(tagnum: &String) {
    let view = "current";
    send_client_message(&format!("toggleview {} {}", view, tagnum), false);
}

fn switchview(viewid: &String) {
    send_client_message(&format!("switch {}", viewid), false);
}

fn changemode(newmode: &String) {
    send_client_message(&format!("mode {}", newmode), false);
}

fn quit() {
    send_client_message("quit", false);
}

fn ping() {
    send_client_message("ping", true);
}

fn querytag() {
    let output = Command::new("bspc")
        .arg("query")
        .arg("-N")
        .arg("focused")
        .arg("-n")
        .output()
        .unwrap();
    let node = std::str::from_utf8(output.stdout.as_slice()).unwrap().trim();
    send_client_message(&format!("querytag {}", node), true);
}

fn queryview() {
    let view = "current";
    send_client_message(&format!("queryview {}", view), true);
}

fn queryactive() {
    send_client_message("queryactive", true);
}

fn querymode() {
    send_client_message("querymode", true);
}

fn querycurrentview() {
    send_client_message("querycurrentview", true);
}

fn main() {
    let mut args = env::args().skip(1);
    match args.nth(0).unwrap().as_str() {
        "server" => server(),
        "tag" => tag(&args.nth(0).unwrap()),
        "toggletag" => toggletag(&args.nth(0).unwrap()),
        "nextempty" => nextemptytag(),
        "view" => view(&args.nth(0).unwrap()),
        "toggleview" => toggleview(&args.nth(0).unwrap()),
        "switch" => switchview(&args.nth(0).unwrap()),
        "mode" => changemode(&args.nth(0).unwrap()),
        "quit" => quit(),
        "ping" => ping(),
        "querytag" => querytag(),
        "queryview" => queryview(),
        "queryactive" => queryactive(),
        "querymode" => querymode(),
        "querycurrentview" => querycurrentview(),
         _ => panic!("wrong argument"),
    }
}
