# Nalu, or (dwm|wmii|awesomewm)-like tags for bspwm

Hello, dear adventurer of the open-source universe !

## What is this

The goal of nalu is to enhance bspwm with the blessed tag system found in wmii,
dwm as well as awesomewm, with views addition. In short, it deals with nodes visibility
by doing grouped hide/show actions.

## How does it work

Nalu keeps track of desktops and nodes within desktops.

Each node is given a set of tag, called a tagset.

For each desktop, several sets of tags, called views, are defined.
A view is literally a tagset.

Inside each desktop, you see one and only one view at any moment.
That view is called the current view.

A node is visible in the current view either:

1. If it's tagset is empty
2. If it's tagset has a non-empty intersection with the current view's tagset

The nodes visibility are managed through `bspc node <node> -g hidden=<visibility>` calls.
There is a bit of smartness when changing view's or node's tagset in order to make
the fewest calls possible.

### How is it different from desktops ?

Desktops' node ownership is mutually exclusive, tags' isn't.
You can view several tags at the same time, and a node can belong
to several tags at the same time.

### Is it really like dwm tags ? You explained it in a weird manner ...

Oh really ? Even though I used the `tagset` keyword ?

In dwm, there are no notion of desktops, so you can consider
you only have one desktop per monitor.

Also in dwm, you have only one view (well, actually two because you can alt-tab with the previous one).

The rest of the concept is the same.

## Useful details

### Node creation

A node is created with an empty tagset, so it will be visible by default.
If you wish to make some nodes tagged to something by default, just plug the correct nalu command
in a bspwm external rule.

### Transfering nodes

A node being swapped or transferred keeps its tagset.
While this is perfectly expected when moving a node within a given desktop,
it is also the case when moving a node from a desktop to another.

Consequently, if the tags

### Untagged node == unmanaged node

A node with an empty tagset is in fact not really managed by nalu.
So you *can* (but maybe shouldn't ?) mix nalu with other code you may have to handle
node visibility. Just make sure you never tag a node which should be handled by another
utility. Otherwise, welcome to hell.

### Nalu exit

When quitting nalu with `nalu quit`, all the tagged nodes are made visible.
This way you can see, focus, and easily perform action on them.

If nalu exits without restoring hidden nodes, take these actions:

1. `for n in $(bspc query -N); do bspc node $n -g hidden=off; done` to make everyone appear.
2. Try to reproduce.
3. Drop an issue, coz' that's a bug !

### Hardcoded constants

You have 10 views for each desktop, indexed from 1 to 10.
A tagset is a bit array coded on a u64, so you have 64 tags, indexed from 1 to 64.

## How to use

Nalu uses a client-server model.

For now, both the client and server are in the same executable,
and they communicate through an ugly hardcoded FIFO file.

To start using nalu, launch the server with `nalu server`.
It won't fork.
It will track the nodes and desktops that are currently there.

In the following commands, `<tagset>` can be formatted in two ways:

1. An integer (between 1 and 64): you will target the n-th tag.
2. An hexadecimal integer with the 0x prefix: you will target the tagset corresponding to that bit array.

Example:

- To view tag number 1, you can do `nalu view 1` OR `nalu view 0x1`.
- To view tag number 5, you can do `nalu view 5` OR `nalu view 0x10`.
- To view tag number 1 AND tag number number 2 in one go, it's `nalu view 0x3`.

You can also do the later example in two commands:
```
nalu view 1
nalu toggleview 2
```

### Manipulating views

To switch to another view:

```
nalu switch <new_view_index>
```

The `new_view_index` is now your current view.


To view one particular tagset on the current view:

```
nalu view <tagset>
```

You can also toggle things:

```
nalu toggleview <tagset>
```

Manipulating tags of another view than the current is currently not available
(but not too complicated to hack & merge if you need it).

### Manipulating nodes

This is even simpler than for views:

```
nalu tag <tagset>
nalu toggletag <tagset>
```

For now the manipulated node is the focused one, but it's quite simple
to hack & merge that if you need it.

### Exiting

```
nalu quit
```
